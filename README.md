# Pdf2Image

#### 介绍
pdf图片转图片工具，课设置dpi（清晰度），选择图片类型（jpg，png），自动遍历子目录，可视化页面操作

#### 软件架构
jdk1.8 + Maven + awt


#### 使用说明

1. 程序入口方法： com.groupies.Main.Pdf2ImageIteratorWindowsAdaptive;
![输入图片说明](src/main/java/com/groupies/image.png)

